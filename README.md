## Bash scripting, SSH and SCP

### Bash

Bash scripting is the ability to declaritively type bash commands that provision a machine on a file and then running it against a machine.

Provisioning a machine includes:

- Making files and directories
- Editing/configuring files
- Installing software
- Starting and stopping files 
- Creating init files
- Sending files and code over to computer (SCP)
  

### SSH

SSH, secure shell, is very useful to securely log into a computer from a distance. It allows us to open a terminal on said computer with shell. We can then use all bash knowledge to configure the machine. 

Main commands:

```bash
# Remote log in to a machine
# syntax ssh <option> <user>@machine.ip

# Example:
$ ssh -i ~/.ssh-mykey.pem ubuntu@34.21.23.4
$ ssh -i ~/.ssh-mykey.pem amalia@34.21.23.4
```

SSH also allows you to run commands remotely.

For example:

```bash
# ls in a remote machine
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls

# You can add the command after the ssh user and machine
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls demos
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls cat bad.
txt

# This is supposing that you have a file or directory named demos and another named bad.txt - this'll be different on yours.

# Create files in the machine from your own
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 touch amalia.md
```


### SCP- secure copy into a prime location

Imagine 'mv' but with ssh keys and remote computers:

Used to move files and/or folders into remote machines. This is useful to move .sh files or folders with code that needs to be set up.

```bash
# Syntax
# scp -i ~/..ssh/ch9_shared.pem <source/file> <target/file>
# scp -i ~/..ssh/ch9_shared.pem <source/file> <user>@<ip>:<path/to/location>

$ scp -i ~/.ssh/ch9_shared.pem /Users/Amalia/Code/week_2/bashscriptin_ssh_scp/bash_script101.sh ubuntu@54.229.20.180:/home/ubuntu/scp_test

# Syntax to send folders

$ scp -i ~/.ssh/ch9_shared.pem -r amalia_website ubuntu@54.229.20.180:/home/ubuntu/scp_test
```

